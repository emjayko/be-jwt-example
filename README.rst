========================
 Falcon/JWT example app 
========================

Simple Falcon app with one dummy endpoint for demonstration of work with JWT.

How to use
==========

Setup app::

   python3 -m venv venv
   . venv/bin/activate
   pip install -r requirements.txt

Start app::

   gunicorn app:api

Get JWT token for one of users::

   export BASIC=$(echo -n "john:jon123" | base64)
   curl -H "Authorization: Basic $BASIC" http://127.0.0.1:8000/token

Authenticate with JWT token (from result of previous request)::

   curl -H "Authorization: Bearer {access_token}" http://127.0.0.1:8000/me
