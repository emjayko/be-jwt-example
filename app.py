import base64

from datetime import (
    datetime,
    timedelta
)

import falcon
import jwt


TOKEN_EXPIRATION = 120  # zivotnost JWT tokenu v sekundach

SECRET_KEY = '09d2af8dd22201dd8d48e5dcfcaed281ff9422c7'

# akoze databaza pouzivatelov
USERS = {
    1: {
        'id': 1,
        'name': 'John',
        'surname': 'Doe',
        'login': 'john',
        'password': 'jon123'
    },
    2: {
        'id': 2,
        'name': 'Jane',
        'surname': 'Doe',
        'login': 'jane',
        'password': 'jane987'
    },
}


class AuthMiddleware:

    def __init__(self, allowed_paths=[]):
        # nastavime si endpointy bez autentifikacie
        self._allowed_paths = allowed_paths

    def process_request(self, req, resp):
        # ak ideme na neoverovay endpoint preskocime dalsie overovanie
        if req.path in self._allowed_paths:
            return
        # ak neprisla Authorization hlacivka, alebo v nej nie je token s typom
        # Bearer, vratime chybu
        if not req.auth or req.auth.split()[0] != 'Bearer':
            raise falcon.HTTPError(falcon.HTTP_401)
        # skusime overit token, ak overenie zlyha, vratime chybu
        try:
            jwt_data = jwt.decode(req.auth.split()[1],
                                  SECRET_KEY,
                                  algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise falcon.HTTPError(falcon.HTTP_403)
        # ak overenie prejde bez chyby, ulozime to kontextu requestu id
        # pouzivatela, ktore mozme nasledne pouzit v resource
        else:
            req.context['user_id'] = int(jwt_data['sub'])


# iba pomocna funkcia pre najdenie pouzivatela podla loginu a hesla,
# realne to bude riesit ORM
def authenticate(login, password):
    user = filter(lambda u: u['login'] == login and u['password'] == password,
                  USERS.values())
    user = list(user)
    return user[0] if user else None


class TokenResource:
    def on_get(self, req, resp):
        # pre vystvenie tokenu potrebujeme autentifikovat pouzivatela
        # menom a heslom, takze ak nemame Authorization hlavicku, vratime chybu
        if not req.auth:
            raise falcon.HTTPError(falcon.HTTP_400)

        auth_type, auth_token = req.auth.split(' ')

        # ocakavame Basic autentifikaciu, inac vratime chybu
        if not auth_type == 'Basic':
            raise falcon.HTTPError(falcon.HTTP_400)

        # z hlavicky ziskame login, heslo a skusime podla nich
        # najst pouzivatela
        login, password = base64.b64decode(auth_token).decode().split(':')
        user = authenticate(login, password)

        # ak sme nenasli pouzivatela, vratime chybu
        if not user:
            raise falcon.HTTPError(falcon.HTTP_403)

        # pripravime si data na token
        jwt_data = {
            'iss': 'myapp',
            'exp': datetime.utcnow() + timedelta(seconds=TOKEN_EXPIRATION),
            'sub': str(user['id'])
        }

        # vygenerujeme token
        jwt_token = jwt.encode(jwt_data, SECRET_KEY, algorithm='HS256')

        # vratime token podla two-legged OAuth 2.0
        resp.media = {
            'access_token': jwt_token.decode(),
            'expires_in': TOKEN_EXPIRATION,
            'token_type': 'Bearer'
        }


class UserResource:
    def on_get(self, req, resp):
        # nacitame pouzivatela podla jeho ID z kontextu requestu
        user = USERS[req.context['user_id']]

        # vratime detaily o pouzivatelovi
        resp.media = {
            'id': user['id'],
            'name': user['name'],
            'surname': user['surname']
        }


api = falcon.API(middleware=AuthMiddleware(['/token']))
api.add_route('/token', TokenResource())
api.add_route('/me', UserResource())
